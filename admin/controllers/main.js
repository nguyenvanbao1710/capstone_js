const renderProductsListService = function () {
  productsServ
    .getList()
    .then((res) => {
      exportProductsList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
renderProductsListService();
const exportProductsList = function (data) {
  let contentHTML = "";
  data.forEach(function (item) {
    let contentTr = `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td>${item.image}</td>
            <td>${item.description}</td>
            <td>            
                <span class="mr-3"  onclick="editProduct(${item.id})" 
                data-toggle="modal"
                data-target="#myModal"
                ><i class="fa fa-edit"></i></span>
                <span  onclick="deleteProduct(${item.id})"><i class="fa fa-times text-danger"></i></span>
            </td>
        </tr>
        `;
    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachSanPham").innerHTML = contentHTML;
};

const deleteProduct = function (id) {
  axios({
    url: `${BASE_URL}/san-pham/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderProductsListService();
    })
    .catch((err) => {
      console.log(err);
    });
};

const getInfor = function () {
  let name = document.getElementById("TenSanPham").value;
  let price = document.getElementById("GiaSanPham").value;
  let image = document.getElementById("HinhAnh").value;
  let description = document.getElementById("MoTa").value;
  return new Products(name, price, image, description);
};

let validatorProduct = new ValidatorProducts();

const addProducts = function () {
  let newProduct = getInfor();
  let isValid = true;
  let isValidName =
    validatorProduct.validateBlanks(
      "TenSanPham",
      "tbTenSanPham",
      "Tên sản phẩm chưa nhập!"
    ) && validatorProduct.validName("TenSanPham", "tbTenSanPham");
  let isValidPrice =
    validatorProduct.validateBlanks(
      "GiaSanPham",
      "tbGiaSanPham",
      "Giá sản phẩm chưa nhập kìa!"
    ) && validatorProduct.validPrice("GiaSanPham", "tbGiaSanPham");

  let isValidImage =
    validatorProduct.validateBlanks(
      "HinhAnh",
      "tbHinhAnh",
      "Hình sản phẩm còn chưa nhập nữa!"
    ) && validatorProduct.validImage("HinhAnh", "tbHinhAnh");
  let isValidDesc = validatorProduct.validateBlanks(
    "MoTa",
    "tbMoTa",
    "Mô tả chưa viết sao được!"
  );

  isValid = isValidName && isValidPrice && isValidImage && isValidDesc;
  if (isValid) {
    axios({
      url: `${BASE_URL}/san-pham`,
      method: "POST",
      data: newProduct,
    })
      .then((res) => {
        $("#myModal").modal("hide");
        renderProductsListService();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

const renderInfor = function (data) {
  document.getElementById("TenSanPham").value = data.name;
  document.getElementById("GiaSanPham").value = data.price;
  document.getElementById("HinhAnh").value = data.image;
  document.getElementById("MoTa").value = data.description;
};

const editProduct = function (id) {
  axios({
    url: `${BASE_URL}/san-pham/${id}`,
    method: "GET",
  })
    .then((res) => {
      renderInfor(res.data);
      document.querySelector("#id-sp span").innerHTML = res.data.id;
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateProducts = function () {
  let updateProduct = getInfor();
  let idProduct = document.querySelector("#id-sp span").innerHTML * 1;
  axios({
    url: `${BASE_URL}/san-pham/${idProduct}`,
    method: "PUT",
    data: updateProduct,
  })
    .then((res) => {
      $("#myModal").modal("hide");
      renderProductsListService();
    })
    .catch((err) => {
      console.log(err);
    });
};
