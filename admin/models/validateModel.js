function ValidatorProducts() {
  this.validateBlanks = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    }
    document.getElementById(idError).innerText = "";
    return true;
  };
  this.validName = function (idTarget, idError) {
    let parten = /[A-Za-zÀ-ÖØ-öø-ÿ]/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Tên sản phẩm phải là chữ";
  };
  this.validPrice = function (idTarget, idError) {
    let parten = /^[0-9]+$/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Giá sản phẩm phải là số";
  };
  this.validImage = function (idTarget, idError) {
    let parten = /.(?:jpg|gif|png)$/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Hình sản phẩm phải có dạng (.jpg .png .gif)";
  };
}
